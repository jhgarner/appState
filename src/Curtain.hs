{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}

module Curtain where

import Control.Monad.State.Strict
import Data.Functor.Identity
import Data.Map
import Debug.Trace
import GHC.Exts

-- S is the actual computation we want to run. This could be replaced with
-- something more generic if we wanted to pull the global state from, for
-- example, a database or something.
type S a = StateT (Map String Int) Identity a

data AState a = AState
  { inputs :: [String],
    outputs :: [String],
    comp :: S a
  }
  deriving (Functor)

instance Applicative AState where
  pure = AState [] [] . pure
  AState ia oa ca <*> AState ib ob cb =
    AState (ia ++ ib) (oa ++ ob) (ca <*> cb)

instance IsString (AState Int) where
  fromString s = AState [s] [] $ gets (! s)

set :: String -> AState Int -> AState ()
set s AState {..} = AState inputs (s : outputs) $ comp >>= (modify' . insert s)

-- Other versions of `run` could do more interesting things like distribute the
-- computations across different threads and shuffle the global state around
-- without needing locks.
run :: [AState ()] -> Map String Int
run ls = execState (traverse comp ls) mempty

-- TODO An actual implementation of a topological sort would go here.
topoSort :: [AState a] -> [AState a]
topoSort = id
