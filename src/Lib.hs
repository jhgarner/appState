{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}

module Lib
    ( someFunc
    ) where

import Curtain
import Control.Applicative


-- AState represents a computation which reads and writes to global state. How
-- is AState different from State? Since AState only implements Applicative,
-- you can statically analyze which parts of the global state a computation
-- uses.
--
-- You can pretend that each string represents a global variable name in the
-- functions below. Haskell's applicative syntax is a little verbose so it's
-- kind of hard to read. If we were using something like Idris, it would
-- probably be even nicer.

addBoth :: AState ()
addBoth = set "z" $ liftA2 (+) "x" "y"

-- Multiplies `a` by 5 and stores the value in `x` and `w`.
mul5 :: AState ()
mul5 = set "x" (liftA2 (*) "a" (pure 5)) *> set "w" "x"

initial :: AState ()
initial = set "a" (pure 10) *> set "y" (pure 3)

someFunc :: IO ()
someFunc = print $ run $ topoSort [initial, mul5, addBoth]
